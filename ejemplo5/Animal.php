<?php

/**
 * Description of Animal
 *
 * @author Profesor Ramon
 */

namespace ejemplo5;
        
class Animal {
    public $tipo;
    public $nombre;
    
    public function getTipo() {
        return $this->tipo;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setTipo($tipo): void {
        $this->tipo = $tipo;
    }

    public function setNombre($nombre): void {
        $this->nombre = $nombre;
    }
    
    public function datos(){
        echo "<ul>";
        echo "<li>El tipo de Animal es " . $this->tipo . "</li>";
        //echo "<li>El nombre es " . $this->nombre . "</li>";
        echo "<li>El nombre es {$this->nombre} </li>";
        echo "</ul>";
    }
    
    


}
